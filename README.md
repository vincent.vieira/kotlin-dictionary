# Simple Kotlin Dictionary application

## About
This is inspired by *Packt Publishing* and one of their great book, [Kotlin Blueprints](https://github.com/PacktPublishing/Kotlin-Blueprints/).
The goal here is to use the code of the last part of the book, refactor it using clean code good practices while adding tests, of course.

### Already done tasks
- Some JavaFX component design on SearchForm with event emission in order to trigger a search
- Add support for triggering a search straight away when pressing enter while text input is focused
- Making application configuration injectable through [Koin Dependency Injection framework](https://insert-koin.io/)

### Tasks
- Making the Meaning fetching abstract, injected through a DI container, with an implementation supporting HTTP calls in order to mock them
properly during tests
- Decoupling JSON Models from ViewModels
- Completely remove any HTTP API configuration from the MainView class
- Decoupling the Meaning result formatting from the MainView to its proper component
- Implement headless tests for some TornadoFX components

### Challenges
No more than JUnit and [MockK](https://mockk.io/) for tests ! `koin-test` is forbidden (and unnecessary, by the way)

### Requirements
This project requires OpenJFX or JavaFX installed.

This project also requires [an API key from RapidAPI](https://docs.rapidapi.com/docs/keys) corresponding to the
WordsAPI in order to request the [definitions](https://rapidapi.com/dpventures/api/wordsapi?endpoint=54b86419e4b058c0230e421c) endpoint.
