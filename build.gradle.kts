import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.72"
    id("org.openjfx.javafxplugin") version "0.0.9"
}

group = "io.vieira"
version = "1.0.0"

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("no.tornado:tornadofx:1.7.20")
    implementation("org.apache.httpcomponents:httpclient:4.5.12")
    implementation("com.natpryce:konfig:1.6.10.0")
    implementation("org.koin:koin-core:2.1.6")

    testImplementation("org.junit.jupiter:junit-jupiter:5.6.2")
    testImplementation("org.testfx:testfx-junit5:4.0.16-alpha")
    testImplementation("org.assertj:assertj-core:3.16.1")
}

javafx {
    version = "13"
    modules("javafx.controls")
}

tasks {
    test {
        useJUnitPlatform()
    }

    withType<KotlinCompile>().configureEach {
        kotlinOptions.jvmTarget = "1.8"
    }
}
