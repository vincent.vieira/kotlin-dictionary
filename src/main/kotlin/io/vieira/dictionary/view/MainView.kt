package io.vieira.dictionary.view

import io.vieira.dictionary.AppConfiguration
import io.vieira.dictionary.controller.WordController
import io.vieira.dictionary.search.SearchForm
import io.vieira.dictionary.search.SearchMeaningRequest
import io.vieira.dictionary.view.Styles.Companion.cssRule
import javafx.scene.control.Label
import javafx.scene.layout.VBox
import tornadofx.*

class MainView : View("Dictionary") {

    private val api: Rest by inject()

    private val controller: WordController by inject()

    private var result: Label by singleAssign()

    override val root: VBox

    private var searchForm: SearchForm by singleAssign()

    private val configuration: AppConfiguration by di()

    init {
        api.baseURI = "https://wordsapiv1.p.rapidapi.com/words/"
        api.engine.requestInterceptor = {
            it.addHeader("X-Mashape-Key", configuration.apiKey)
        }

        searchForm = SearchForm()

        root = vbox {
            addClass(cssRule)

            result = Label()

            this += searchForm
            this += result
        }

        subscribe<SearchMeaningRequest> { event ->
            runAsync { controller.getMeaning(event.searchTerm) } ui { meaning ->
                result.text = "Meaning(s): " + (meaning?.let { m ->
                    m.definitions.joinToString(
                        "\n • ",
                        "\n • "
                    ) { it.definition }
                } ?: "Unable to find the meaning")

            }
        }
    }
}
