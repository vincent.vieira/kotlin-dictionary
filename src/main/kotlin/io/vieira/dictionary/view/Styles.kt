package io.vieira.dictionary.view

import tornadofx.Stylesheet
import tornadofx.cssclass
import tornadofx.px

class Styles : Stylesheet() {
    companion object {
        val cssRule by cssclass()
    }

    init {
        select(cssRule) {
            minHeight = 200.px
            minWidth = 300.px
        }
    }
}
