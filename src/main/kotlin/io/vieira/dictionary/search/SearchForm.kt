package io.vieira.dictionary.search

import javafx.event.EventHandler
import javafx.scene.control.TextField
import javafx.scene.input.KeyCode
import tornadofx.*

class SearchForm : Fragment() {

    override val root = Form()

    private var fieldSet: Fieldset by singleAssign()

    private var input: TextField by singleAssign()

    init {
        fieldSet = Fieldset(labelPosition = javafx.geometry.Orientation.VERTICAL)

        with(root) {
            with(fieldSet) {
                this += field("Enter word", javafx.geometry.Orientation.VERTICAL) {
                    input = textfield {
                        onKeyReleased = EventHandler { e ->
                            if (e.code == KeyCode.ENTER) {
                                triggerSearch(input.text)
                            }
                        }
                    }
                }

                this += buttonbar {
                    button("Get meaning").action {
                        triggerSearch(input.text)
                    }
                }
            }

            this += fieldSet
        }
    }

    private fun triggerSearch(text: String) {
        fire(SearchMeaningRequest(text))
    }
}
