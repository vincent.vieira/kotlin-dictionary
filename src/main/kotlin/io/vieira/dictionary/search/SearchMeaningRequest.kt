package io.vieira.dictionary.search

import tornadofx.EventBus
import tornadofx.FXEvent

class SearchMeaningRequest(val searchTerm: String) : FXEvent(EventBus.RunOn.BackgroundThread)
