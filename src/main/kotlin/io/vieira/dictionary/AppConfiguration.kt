package io.vieira.dictionary

import com.natpryce.konfig.ConfigurationProperties.Companion.fromResource
import com.natpryce.konfig.ConfigurationProperties.Companion.systemProperties
import com.natpryce.konfig.EnvironmentVariables
import com.natpryce.konfig.getValue
import com.natpryce.konfig.overriding
import com.natpryce.konfig.stringType

object AppConfigurationProperties {
    val apiKey by stringType
}

object AppConfiguration {
    private val configuration = systemProperties() overriding
            EnvironmentVariables() overriding
            fromResource("configuration.properties")

    val apiKey = configuration[AppConfigurationProperties.apiKey]
}
