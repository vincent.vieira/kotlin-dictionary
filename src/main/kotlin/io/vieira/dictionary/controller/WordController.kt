package io.vieira.dictionary.controller

import io.vieira.dictionary.model.Meaning
import tornadofx.Controller
import tornadofx.Rest
import tornadofx.toModel

class WordController : Controller() {

    private val api: Rest by inject()

    fun getMeaning(word: String): Meaning? {
        val response = api.get("$word/definitions")
        try {
            return when {
                response.ok() -> response.one().toModel()
                else -> null
            }
        } finally {
            response.consume()
        }
    }
}
