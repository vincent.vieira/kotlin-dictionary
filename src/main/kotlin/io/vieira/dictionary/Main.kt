package io.vieira.dictionary

import io.vieira.dictionary.view.MainView
import io.vieira.dictionary.view.Styles
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.qualifier.named
import org.koin.dsl.module
import tornadofx.*
import kotlin.reflect.KClass

class DictionaryApp : App(MainView::class, Styles::class) {

    override fun init() {
        Rest.useApacheHttpClient()
        FX.dicontainer = object : DIContainer, KoinComponent {
            override fun <T : Any> getInstance(type: KClass<T>): T =
                getKoin().get(clazz = type, parameters = null, qualifier = null)

            override fun <T : Any> getInstance(type: KClass<T>, name: String): T =
                getKoin().get(clazz = type, parameters = null, qualifier = named(name))
        }
        configureKoin()
    }

    override fun stop() {
        stopKoin()
        super.stop()
    }
}

private fun configureKoin() = startKoin {
    modules(
        module {
            single { AppConfiguration }
        }
    )
}

fun main(args: Array<String>) {
    launch<DictionaryApp>(args)
}
